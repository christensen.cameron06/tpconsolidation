import pytest

def test_2_egal_2():
  assert 2==2

from main import isVegan


def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for ingredient in ingredients:
        if 'vegan' in ingredient:
          if ingredient["vegan"]=='no' or ingredient["vegan"]=='maybe':
            return False
    return True

def test_rozana_isVegan_true():
    rozana_data=load_params_from_json('rozana.json')
    assert isVegan(rozana_data) == True

def test_brioche_isVegan_false():
    brioche_data=load_params_from_json('brioche.json')
    assert isVegan(brioche_data) == False
